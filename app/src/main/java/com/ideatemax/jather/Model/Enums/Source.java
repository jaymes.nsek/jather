package com.ideatemax.jather.Model.Enums;

public enum Source {
    SHARED_PREFERENCES,
    NETWORK
}
