package com.ideatemax.jather.Model.DTOs;

import androidx.annotation.NonNull;
import com.ideatemax.jather.Model.Enums.DTOType;

/**
 * Class holds all information pertaining to a particular city's weather such as name,
 * tempInCelsius, humidity, and weather description.
 */
public class CityDTO {

    public static DTOType dtoType = DTOType.city;

    public String name;
    public float tempInCelsius;
    public int humidity;
    public String weatherDescription;
    // The time that the report was issued.
    public long timeInUnixSeconds;
    //public int mSharedPreferenceIndex = -1;

    public CityDTO(){ }

    /**
     * Secondary Constructor
     * @param name is the name of the city.
     * @param tempInCelsius is the temperature of the city in Fahrenheit.
     * @param humidity is the humidity percentage.
     * @param weatherDescription is the accompanying description of the weather phenomena.
     * @param timeInUnixSeconds is the time that the report was issued.
     */
    public CityDTO(String name, float tempInCelsius, int humidity, String weatherDescription,
                   long timeInUnixSeconds) {
        this.name = name;
        this.tempInCelsius = tempInCelsius;
        this.humidity = humidity;
        this.weatherDescription = weatherDescription;
        this.timeInUnixSeconds = timeInUnixSeconds;
    }

    @NonNull
    @Override
    public String toString() {
        return "CityDTO{" +
                "name='" + name + '\'' +
                ", tempInCelsius=" + tempInCelsius +
                ", humidity=" + humidity +
                ", weatherDescription='" + weatherDescription + '\'' +
                ", timeInUnixSeconds=" + timeInUnixSeconds +
                '}';
    }

}
