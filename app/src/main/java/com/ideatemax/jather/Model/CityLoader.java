package com.ideatemax.jather.Model;

import android.content.Context;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.loader.content.AsyncTaskLoader;
import com.ideatemax.jather.Model.Enums.Info;
import com.ideatemax.jather.Model.Network.APIComms.OnServerResultsListener;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import static com.ideatemax.jather.Model.Network.APIComms.OnServerResultsListener.FAILED;
import static com.ideatemax.jather.Model.Network.NetworkUtils.makeHttpRequest;


public class CityLoader extends AsyncTaskLoader<List<String>> {

    private final String TAG = getClass().getSimpleName();
    private URL[] citiesURLArr;
    private List<String> responseList = null;
    private String[] response = null;
    private int responseCode;
    private boolean hasDataChanged = false;
    private OnServerResultsListener serverResultsListener;

    private int successful = 0;
    private int failed = 0;

    public CityLoader(@NonNull Context context,
                      URL[] citiesURLArr,
                      OnServerResultsListener listener) {
        super(context);
        this.citiesURLArr = citiesURLArr;
        serverResultsListener = listener;
    }

    //region Loader Overrides
    @Nullable
    @Override
    public List<String> loadInBackground() {
        if (citiesURLArr.length == 0) return null;
        String jsonResponse = "";
        responseList = new ArrayList<>();

        for (URL url : citiesURLArr){
            if (url == null) continue;

            try {
                response = makeHttpRequest(url);
                jsonResponse = response[0];
                responseCode = Integer.parseInt(response[1]);
                Log.i(TAG, "loadInBackground: " + responseCode);
            } catch (IOException e) {
                Log.e(TAG, "Error making a connection", e);
            } finally {
                if (jsonResponse.length() > 10) responseList.add(jsonResponse);
            }
        }
        Log.i(TAG, "loadInBackground: jsonRes - " + responseList);
        return responseList;
    }

    @Override
    protected void onStartLoading() {
        Log.i(TAG, "onStartLoading: isReset:"+ isReset() + " isStarted:" + isStarted() +
                " ContentChange:"+ takeContentChanged());
        // If there is currently a result available, deliver it immediately.
        if (responseList != null){
            Log.i(TAG, "onStartLoading: responseList returning as... " + responseList.toString());
            deliverResult(responseList);
        }
        else {
            Log.i(TAG, "onStartLoading: forceLoad() called");
            forceLoad();
        }
    }

    @Override
    public void deliverResult(@Nullable List<String> data) {
        super.deliverResult(data);
    }

    //endregion

    /**
     * Method used in conjunction with responseCode to determine whether all request was
     * successful, failed, or partially successful.
     */
    private void monitorResponseCode(int responseCode){
        if (responseCode == FAILED) failed++;
        else successful++;
    }

    private Info returnAndReset(){
        Info info;
        if (successful > 0 && failed == 0){
            info = Info.SUCCESS;
        }
        // TODO: Change below cases to reflect their significance, add new param to Info.
        else  if (failed > 0 && successful == 0){
            info = Info.RETURNED_NULL;
        }
        else {
            info = Info.RETURNED_NULL;
        }

        successful = 0;
        failed = 0;
        return info;
    }

}
