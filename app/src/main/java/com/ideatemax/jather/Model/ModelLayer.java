package com.ideatemax.jather.Model;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.core.util.Consumer;
import androidx.loader.app.LoaderManager;

import com.ideatemax.jather.Activities.CityList.CityView;
import com.ideatemax.jather.Activities.CityList.CityViewMini;
import com.ideatemax.jather.Model.DTOs.CityDTO;
import com.ideatemax.jather.Model.Data.CitiesList.CitiesArrayList;
import com.ideatemax.jather.Model.Enums.Info;
import com.ideatemax.jather.Model.Enums.ProcessType;

import java.util.List;

public interface ModelLayer {
    void loadData(CityViewMini viewMini);
    void loadCityNames(List<String> names);
    void setProcessTypeAndNames(ProcessType type, @Nullable List<String> names);
    boolean removeNameFromSharedPref(String name);
    boolean isCityInSharedPref(String name);
    List<String> getNamesInSharedPref();
}
