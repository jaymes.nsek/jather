package com.ideatemax.jather.Model.Data;

import android.content.SharedPreferences;
import android.util.Log;
import androidx.core.util.Consumer;
import com.ideatemax.jather.Helpers.Constants;
import com.ideatemax.jather.Model.Data.SharedPref.SharedPreferenceable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class DataLayerImpl implements  DataLayer, SharedPreferenceable {

    private final String TAG = getClass().getSimpleName();
    static boolean DEBUG = true;

    private SharedPreferences preferences;
    private static List<String> currentNamesInSharedPref = null;

    public DataLayerImpl(SharedPreferences preferences) {
        this.preferences =  preferences;
    }

    @Override
    public void loadCityNamesFromLocal(Consumer<List<String>> onResults) {
        loadNamesFromSPref(onResults);
    }

    @Override
    public boolean removeNameFromSharedPref(String name) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(name);
        editor.apply();
        return true;
    }

    private void loadNamesFromSPref(Consumer<List<String>> onResults) {
        List<String> cityNames = getNamesInSharedPref();

        try {
            onResults.accept(cityNames);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<String> getNamesInSharedPref() {
        try{
            Map<String, ?> rawList =  preferences.getAll();
            List<String> nameList = new ArrayList<>(rawList.keySet());
            nameList.remove(Constants.index_generator_key_FS);
            logList(nameList);
            currentNamesInSharedPref = nameList;
            return nameList;
        }catch (NullPointerException e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public String preferencesToString() {
        return "SharedPreferences{" +
                "indexAndFieldsSPREF=" + preferences.getAll() +
                '}';
    }

    private void logList(List<String> citiesNameList){
        StringBuilder builder = new StringBuilder();
        for (String name : citiesNameList){
            builder.append(name).append(" ");
        }
        Log.i(TAG, "logList - SharedPref contents:" + builder);
    }

    /**
     * Method returns true if mCity name is already stored.
     */
    public boolean isCityInSharedPref(String name) {
        // Call method to update names held in currentNamesInSharedPref field.
        getNamesInSharedPref();
        for (String aName : currentNamesInSharedPref) {
            if (name.equalsIgnoreCase(aName)) return true;
        }
        return false;
    }

}
