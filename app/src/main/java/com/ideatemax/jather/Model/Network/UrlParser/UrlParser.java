package com.ideatemax.jather.Model.Network.UrlParser;

import android.util.Log;

import androidx.core.util.Supplier;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.ideatemax.jather.Helpers.Constants.OW_API_KEY;
import static com.ideatemax.jather.Helpers.Constants.OW_METRIC_FORMAT;
import static com.ideatemax.jather.Helpers.Constants.OW_URL_REQUEST_STEM;


public class UrlParser {
    private static final String TAG = "UrlParser";
    private StringBuilder urlString;

    public static UrlParser shared = new UrlParser();


    private UrlParser() { }

    /**
     * Method concatenates and then creates a URL object from the city name and query params.
     * @return the newly created URL object.
     */
    private URL parseURLs(String city){
        urlString = new StringBuilder();

        urlString.append(OW_URL_REQUEST_STEM).append("q=").append(city)
                .append("&").append(OW_METRIC_FORMAT).append("&").append(OW_API_KEY);

        try{
            return new URL( urlString.toString() );
        } catch (MalformedURLException e) {
            Log.e(TAG, "Error creating URL", e);
            return null;
        }
    }

    public List<URL> nameToURL(Supplier<List<String>> namesOfCities) {
        return parseURLs(namesOfCities.get());
    }

    /**
     * Parse all city names into URL.
     */
    private List<URL> parseURLs(List<String> namesOfCities){
        List<URL> citiesURL = new ArrayList<>();

        for(String name : namesOfCities){
           citiesURL.add(parseURLs(name));
        }
        return citiesURL;
    }

}
