package com.ideatemax.jather.Model.Network;

import android.content.Context;

import androidx.core.util.Consumer;
import androidx.core.util.Supplier;
import androidx.loader.app.LoaderManager;

import com.ideatemax.jather.Activities.CityList.CityView;
import com.ideatemax.jather.Activities.CityList.CityViewMini;
import com.ideatemax.jather.Model.DTOs.CityDTO;
import com.ideatemax.jather.Model.Enums.Info;

import java.util.List;


public interface NetworkLayer {
    void loadData(CityViewMini viewMini, Consumer<List<String>> onJsonResults,
                  Supplier<List<String>> namesOfCities);
}
