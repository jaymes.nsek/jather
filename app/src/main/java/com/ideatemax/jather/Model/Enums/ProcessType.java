package com.ideatemax.jather.Model.Enums;

/**
 * Used to differ the paths when App is first launched and multiple Cities are reload or when a
 * single city is added via the AddActivity.
 */
public enum ProcessType {
    SINGLE,
    MULTIPLE,
    REFRESH,
}
