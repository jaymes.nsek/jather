package com.ideatemax.jather.Model;

import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;

import com.ideatemax.jather.Activities.CityList.CityViewMini;
import com.ideatemax.jather.Model.DTOs.CityDTO;
import com.ideatemax.jather.Model.Data.DataLayer;
import com.ideatemax.jather.Model.Data.SharedPref.SharedPreferenceable;
import com.ideatemax.jather.Model.Enums.Info;
import com.ideatemax.jather.Model.Enums.ProcessType;
import com.ideatemax.jather.Model.Network.NetworkLayer;
import com.ideatemax.jather.Translation.TranslationLayer;
import java.util.ArrayList;
import java.util.List;

import static com.ideatemax.jather.Model.Enums.ProcessType.REFRESH;
import static com.ideatemax.jather.Model.Enums.ProcessType.SINGLE;


public class ModelLayerImpl implements  ModelLayer{

    private final String TAG = getClass().getSimpleName();

    private NetworkLayer networkLayer;
    private DataLayer dataLayer;
    private TranslationLayer translationLayer;
    private List<String> namesOfCities;
    private List<String> jsonResponseList = null;
    private ProcessType processType = null;
    private String singleCityName = null;
    //private Consumer<List<CityDTO>> onResults;
    private CityViewMini viewMini;

    public ModelLayerImpl(NetworkLayer networkLayer, DataLayer dataLayer,
                          TranslationLayer translationLayer) {
        this.networkLayer = networkLayer;
        this.dataLayer = dataLayer;
        this.translationLayer = translationLayer;
    }

    @Override
    public void loadData(CityViewMini view) {
        viewMini = view;

        try {
            if (processType == SINGLE  &&  singleCityName != null)
                namesOfCities.add(singleCityName);
            // Handle "on Launch populating"
            else if (processType == ProcessType.MULTIPLE)
                dataLayer.loadCityNamesFromLocal(this::loadCityNames);
            Log.i(TAG, "loadData: " + namesOfCities.toString());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (namesOfCities == null || namesOfCities.size() == 0){
                viewMini.reportFinishStatus(Info.NO_CITY);
                viewMini.onQueryResult(null);
            }
            else{
                Log.i(TAG, "loadData: networkLayer.loadData(...)" + namesOfCities);
                networkLayer.loadData(viewMini, this::onJsonResult, () -> namesOfCities);
            }
        }
    }

    @Override
    public void loadCityNames(List<String> names) {
        this.namesOfCities = names;
    }

    @Override
    public void setProcessTypeAndNames(ProcessType type, @Nullable List<String> names) {
        processType = type;
        // Reset namesOfCities list.
        namesOfCities = new ArrayList<>();

        if (processType == SINGLE && names != null) {
            singleCityName = names.get(0);
            if (names.size() != 1){
                Log.i(TAG, "setProcessTypeAndNames: Illegal size for Process type single");
            }
        }
        else if (processType == REFRESH) {
            namesOfCities = names;
        }
    }

    @Override
    public boolean removeNameFromSharedPref(String name) {
        return dataLayer.removeNameFromSharedPref(name);
    }

    private void onJsonResult(List<String> jsonResponse){
        jsonResponseList = jsonResponse;
        onCityDTOResult(translationLayer.translate(jsonResponseList));
    }

    private void onCityDTOResult(List<CityDTO> cities){
        viewMini.onQueryResult(cities);
    }

    public boolean isCityInSharedPref(String name) {
        return dataLayer.isCityInSharedPref(name);
    }

    @Override
    public List<String> getNamesInSharedPref() {
        SharedPreferenceable sharedPreferenceable = (SharedPreferenceable) dataLayer;
        return sharedPreferenceable.getNamesInSharedPref();
    }
}
