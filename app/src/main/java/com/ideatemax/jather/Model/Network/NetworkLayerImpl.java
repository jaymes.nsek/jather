package com.ideatemax.jather.Model.Network;

import android.content.Context;
import android.util.Log;

import androidx.core.util.Consumer;
import androidx.core.util.Supplier;
import androidx.loader.app.LoaderManager;

import com.ideatemax.jather.Activities.CityList.CityView;
import com.ideatemax.jather.Activities.CityList.CityViewMini;
import com.ideatemax.jather.Model.Enums.Info;
import com.ideatemax.jather.Model.Network.APIComms.APIConnection;
import com.ideatemax.jather.Model.Network.APIComms.APIConnectionImpl;
import com.ideatemax.jather.Model.Network.UrlParser.UrlParser;

import java.net.URL;
import java.util.List;


public class NetworkLayerImpl implements NetworkLayer{

    private final String TAG = getClass().getSimpleName();
    private APIConnection apiConnection;
    private List<URL> namesURLList = null;


    public NetworkLayerImpl() {
        apiConnection = new APIConnectionImpl();
    }

    @Override
    public void loadData(CityViewMini view, Consumer<List<String>> onJsonResult,
                         Supplier<List<String>> namesOfCities){
        namesURLList = UrlParser.shared.nameToURL(namesOfCities);
        loadDataToAPIServerImpl(view, onJsonResult, namesURLList);
    }

    private void loadDataToAPIServerImpl(CityViewMini view,
                                         Consumer<List<String>> onJsonResult,
                                         List<URL> namesURLList){
        apiConnection.loadData(view, onJsonResult, namesURLList);
    }

}
