package com.ideatemax.jather.Model.Data;

import androidx.core.util.Consumer;

import com.ideatemax.jather.Model.DTOs.CityDTO;
import com.ideatemax.jather.Model.Data.CitiesList.CitiesArrayList;

import java.util.List;

public interface DataLayer {
    void loadCityNamesFromLocal(Consumer<List<String>> onResults);
    boolean removeNameFromSharedPref(String name);
    boolean isCityInSharedPref(String name);
}
