package com.ideatemax.jather.Model.Data.SharedPref;

import java.util.List;

public interface SharedPreferenceable {
    List<String> getNamesInSharedPref();
    String preferencesToString();
}
