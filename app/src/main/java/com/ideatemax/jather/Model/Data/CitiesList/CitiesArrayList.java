package com.ideatemax.jather.Model.Data.CitiesList;

import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.util.Supplier;
import com.ideatemax.jather.Model.DTOs.CityDTO;
import java.util.ArrayList;
import java.util.Collection;
import static com.ideatemax.jather.Helpers.Global.DEBUG;


/**
 * Singleton class that holds cities information, such that the data can be mutated from a single
 * instance through out the project.
 */
public final class CitiesArrayList extends ArrayList<CityDTO> {

    private final String TAG = getClass().getSimpleName();

    // Instance variable for registering an implementation Listeners.
    private OnBinarySizeChangeListener mSizeListener;
    private OnUniqueAddListener mUniqueAddListener;
    // Used to shield incompatible methods from use.
    private boolean isCallValid = false;

    public CitiesArrayList() {
        mSizeListener = null;
        mUniqueAddListener = null;
    }

    //region Listener Setters
    /**
     * Listener setter, which allows mSizeListener callbacks to be registered for size change.
     */
    public void setBinarySizeChangeListener(OnBinarySizeChangeListener listener) {
        this.mSizeListener = listener;
        invokeSizeCallback();
    }

    /**
     * Listener setter, which allows mSizeListener callbacks to be registered from parent object.
     */
    public void setUniqueAddListener (OnUniqueAddListener listener) throws NullPointerException {
        this.mUniqueAddListener = listener;
    }
    //endregion


    //region ArrayList Helpers
    /**
     * Method finds the associated name within the ArrayList.
     */
    public CityDTO getCity(String name) {
        for (CityDTO cityDTO : this) {
            // level both mCity names such that they are comparable regardless of cases.
            String aCity = cityDTO.name.toLowerCase();
            String searchCity = name.toLowerCase();

            // If mCity name already exist set return value to true and break.
            if (aCity.equals(searchCity)) {
                return cityDTO;
            }
        }
        return null;
    }

    /**
     * Method returns true if city name is already in ArrayList.
     */
    public boolean isCityInList(String name) {
        for (CityDTO city : this) {
            if (name.equalsIgnoreCase(city.name)) return true;
        }
        return false;
    }
    //endregion

    //region Listener Helpers
    //Helper methods providing sanity check used for triggering callbacks to registered parents.
    private void invokeSizeCallback(){
        if (mSizeListener != null) {
            if (this.size() == 0){
                mSizeListener.isArrayListEmpty(true);
            }
            else if (this.size() == 1)
            {
                mSizeListener.isArrayListEmpty(false);
            }
        }
    }

    private void invokeUniqueAddCallback(String name){
        Log.i(TAG, "invokeUniqueAddCallback: Entered");
        if (mUniqueAddListener != null) {
            Log.i(TAG, "invokeUniqueAddCallback: about to initiate callback");
            mUniqueAddListener.onUniqueAdd(name);
        }
        Log.i(TAG, "invokeUniqueAddCallback: Leaving");
    }
    //endregion

    //region Overrides
    @Override
    public boolean add(CityDTO cityDTO) {
        // Check if mCity name already present, if a refresh is due.
        if (this.isCityInList(cityDTO.name)){
            Log.i(TAG, "add: " + cityDTO.name + " is already in List");
            // isCallValid prevents illegal calls to this.set() being made.
            isCallValid = true;
            boolean returnVal = updateCity(cityDTO);
            isCallValid = false;
            return returnVal;
        }
        else {
            super.add(cityDTO);
            Log.i(TAG, "add: " + cityDTO.name + " not in list");
            invokeUniqueAddCallback(cityDTO.name);
            invokeSizeCallback();
            return true;
        }
    }

    @Override
    public CityDTO set(int index, CityDTO element) {
        if (isCallValid)
            return super.set(index, element);
        else
            throw new IllegalStateException("Illegal call to mutate data, use add() method");
    }

    @Override
    public boolean addAll(@NonNull Collection<? extends CityDTO> c) {
        Log.i(TAG, "addAll: Entered");
        // Adding via addAll() assumes repopulating on launch, uniqueAdd is not checked or invoked.
        for(CityDTO city : c.toArray(new CityDTO[0])) {
            this.add(city);
        }

        // Listener invoked here so that callback is not repeatedly make as List burgeons.
        if (mSizeListener != null){
            if (this.size() > 0)
                mSizeListener.isArrayListEmpty(false);
            else
                mSizeListener.isArrayListEmpty(true);
        }
        Log.i(TAG, "addAll: About to return");
        return true;
    }

    @Override
    public boolean remove(@Nullable Object o) {
        if (DEBUG) {
            assert o != null;
            Log.i(TAG, "remove: called" + o.toString());
        }
        boolean value = super.remove(o);
        invokeSizeCallback();
        return value;
    }

    @Override
    public CityDTO remove(int index) {
        CityDTO city = super.remove(index);
        invokeSizeCallback();
        return city;
    }

    @Override
    public boolean removeAll(@NonNull Collection<?> c) {
        boolean value = super.removeAll(c);
        invokeSizeCallback();
        return value;
    }

    /**
     * Method updates selected fields and is intended for when the old city object is retained.
     */
    private boolean updateCity(CityDTO newCityDTO){
        return manageTask(() -> {
            int positionIndex = this.indexOf(getCity(newCityDTO.name));
            this.set(positionIndex, newCityDTO);
            return true;
        });
    }

    /**
     * Method is intended to be used for various mutation methods that need to be shielded.
     */
    private boolean manageTask(Supplier<Boolean> methodToManage){
        if (isCallValid)
            return methodToManage.get();
        else
            return false;
    }
    //endregion

    //region Nested Interfaces
    /**
     * Interface notifies registered listeners when size is 0 or 1.
     */
    public interface OnBinarySizeChangeListener {
        // The goal is to broadcast TRUE when empty or TRUE on the List attaining size == 1.
        void isArrayListEmpty(boolean state);
    }

    public interface OnUniqueAddListener {
        // Used to return city name on unique add() to registered listener.
        void onUniqueAdd(String name);
    }
    //endregion

}
