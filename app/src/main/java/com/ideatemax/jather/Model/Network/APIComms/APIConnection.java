package com.ideatemax.jather.Model.Network.APIComms;

import android.app.Activity;
import android.content.Context;

import androidx.core.util.Consumer;
import androidx.loader.app.LoaderManager;

import com.ideatemax.jather.Activities.CityList.CityView;
import com.ideatemax.jather.Activities.CityList.CityViewMini;
import com.ideatemax.jather.Model.Enums.Info;

import java.net.URL;
import java.util.List;


public interface APIConnection {
    void loadData(CityViewMini viewMini, Consumer<List<String>> onJsonResult, List<URL> citiesURLList);
}
