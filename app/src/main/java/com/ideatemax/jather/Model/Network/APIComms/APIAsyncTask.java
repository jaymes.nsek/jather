package com.ideatemax.jather.Model.Network.APIComms;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import static com.ideatemax.jather.Model.Network.APIComms.OnServerResultsListener.FAILED;
import static java.net.HttpURLConnection.HTTP_OK;

@Deprecated
public class APIAsyncTask extends AsyncTask<URL, Void, String> {

    private static final String TAG = "APIAsyncTask";
    private static int responseCode = FAILED;
    private OnServerResultsListener listener;


    public APIAsyncTask(OnServerResultsListener listener) {
        this.listener = listener;
    }

    @Override
    protected String doInBackground(URL... names) {
        URL url = names[0];
        String jsonResponse;

        try {
            assert url != null;
            jsonResponse = makeHttpRequest(url);
        } catch (IOException e) {
            Log.e(TAG, "Error making a connection", e);
            return null;
        }

        // Extract relevant fields from JSON response, which returns a CityDTO object with it -
        // Return the CityDTO object as the result for the WeatherAsyncTask.
        return jsonResponse;
    }


    @Override
    protected void onPostExecute(String jsonResponse) {
        listener.responseCodeUpdate(responseCode);
        //listener.onServerResult(jsonResponse);
    }


    /**
     * Methods makes a HTTP request with the given URL to server and returns .
     * @return is the JSON response.
     * @throws IOException if the connection attempt is unsuccessful.
     */
    public static String makeHttpRequest(URL url) throws IOException{
        // Declare variables.
        String jsonResponse = null;
        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;

        try{
            // Open the bridge.
            urlConnection = (HttpURLConnection) url.openConnection();
            // Designate the GET verb as the action.
            urlConnection.setRequestMethod("GET");
            // Set connect time out to 15s (15_000ms)
            urlConnection.setConnectTimeout(15000);
            // Set read time out to 10s (10_000ms)
            urlConnection.setReadTimeout(10000);
            urlConnection.connect();

            responseCode = urlConnection.getResponseCode();
            if (responseCode == HTTP_OK){
                inputStream = urlConnection.getInputStream();
                jsonResponse = readFromInputStream(inputStream);
            }
        } catch (IOException e) {
            Log.e(TAG, "Error retrieving weather JSON", e);
        } finally {
            // If an Exception was thrown in try block, then rCode = -1 (aka FAILED).
            if (responseCode != HTTP_OK) Log.e(TAG, "Error response code: " + responseCode);

            if (urlConnection != null) urlConnection.disconnect();
            if (inputStream != null) inputStream.close();
        }
        return  jsonResponse;
    }


    /**
     * Method reads the inputString and converts it to String.
     * @param inputStream represents an input stream of bytes
     * @return is the JSON response from the server.
     * @throws IOException is thrown if the reading process fails or is interrupted.
     */
    private static String readFromInputStream(InputStream inputStream) throws IOException {
        StringBuilder streamOutput = new StringBuilder();

        if (inputStream != null){
            InputStreamReader inputStreamReader =
                    new InputStreamReader(inputStream, StandardCharsets.UTF_8);
            BufferedReader reader = new BufferedReader(inputStreamReader);

            String line = reader.readLine();
            while (line != null){
                streamOutput.append(line);
                line = reader.readLine();
            }
        }
        return streamOutput.toString();
    }

}
