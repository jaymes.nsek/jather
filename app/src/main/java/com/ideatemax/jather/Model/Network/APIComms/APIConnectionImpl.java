package com.ideatemax.jather.Model.Network.APIComms;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.util.Consumer;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;

import com.ideatemax.jather.Activities.CityList.CityView;
import com.ideatemax.jather.Activities.CityList.CityViewMini;
import com.ideatemax.jather.Model.CityLoader;
import com.ideatemax.jather.Model.DTOs.CityDTO;
import com.ideatemax.jather.Model.Enums.Info;

import java.net.URL;
import java.util.Arrays;
import java.util.List;

import static com.ideatemax.jather.Helpers.Constants.LOADER_ID;
import static java.net.HttpURLConnection.HTTP_OK;

/**
 * Make HTTPRequest via ASyncTask and pass on the InputStream to JsonParser.
 */
public class APIConnectionImpl implements APIConnection, OnServerResultsListener,
        LoaderManager.LoaderCallbacks<List<String>>{

    private final String TAG = getClass().getSimpleName();

    // private List<String> jsonList;
    private Consumer<List<String>> onJsonResult;
    private URL[] citiesURLArr = null;
    private URL[] oldCitiesURLArr = null;
    private List<String> oldResults;
    private CityViewMini view;
    private boolean isRequestMade = false;


    @Override
    public void loadData(CityViewMini viewMini,
                         Consumer<List<String>> onJsonResult, List<URL> citiesURLList) {
        isRequestMade = true;
        setOldCitiesURLArr();
        citiesURLArr = citiesURLList.toArray(new URL[0]);

        view = viewMini;
        this.onJsonResult = onJsonResult;

        // Indicate loading progress
        view.setLoadIndicatorVisibility(View.VISIBLE);

        // If loader does not exist or a duplicate request, call initLoader so that results are
        // returned. If anew request, the restart loader such that the params are passable.
        if (!isDuplicateRequest() || hasSuspectedNetworkError()) {
            Log.i(TAG, "loadData: restartLoader(...)");
            view.getCityLoaderManager().restartLoader(LOADER_ID, null, this);
        }
        else if (view.getCityLoaderManager().getLoader(LOADER_ID) == null || isDuplicateRequest()){
            Log.i(TAG, "loadData: initLoader(...) DupRequest():" + isDuplicateRequest());
            view.getCityLoaderManager().initLoader(LOADER_ID, null, this);
        }

        Log.i(TAG, "loadData: URL Array " + Arrays.toString(citiesURLArr));
    }

    /**
     * Determines whether a URL list received for Loader is the same as the previous request.
     */
    private boolean isDuplicateRequest(){
        boolean isDuplicateCity = false;

        if ( citiesURLArr == null || oldCitiesURLArr == null ||
                citiesURLArr.length != oldCitiesURLArr.length )
            return false;

        for (URL aNewCityURL : citiesURLArr){
            for (URL anOldCityURL : oldCitiesURLArr){
                if (aNewCityURL.toString().equalsIgnoreCase(anOldCityURL.toString())) {
                    isDuplicateCity = true;
                    break;
                }
            }
            if (!isDuplicateCity) return false;
            else isDuplicateCity = false;
        }
        return true;
    }

    /**
     * Method returns true if resultList size does not equal the requestList size.
     */
    private boolean hasSuspectedNetworkError(){
        return oldResults.size() != oldCitiesURLArr.length;
    }

    /**
     * Adopts the previous cities list as oldCitiesURLArr.
     */
    private void setOldCitiesURLArr(){
        if (citiesURLArr == null) {
            oldCitiesURLArr = null;
            return;
        }
        oldCitiesURLArr = new URL[citiesURLArr.length];
        System.arraycopy(citiesURLArr, 0, oldCitiesURLArr, 0, citiesURLArr.length);
    }


    //region OnServerResults Callback
    @Override
    public void responseCodeUpdate(int code) {
        Info info;
        switch (code){
            case OnServerResultsListener.FAILED:
                info = Info.NO_INTERNET;
                break;
            case HTTP_OK:
            default: info = Info.SUCCESS;
        }
        view.reportFinishStatus(info);
    }
    //endregion


    //region  LoaderManager.LoaderCallbacks
    @NonNull
    @Override
    public Loader<List<String>> onCreateLoader(int id, @Nullable Bundle args) {
        Log.i(TAG, "onCreateLoader: " + Arrays.toString(citiesURLArr));
        return new CityLoader(view.getContext(), citiesURLArr, this);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<List<String>> loader, List<String> data) {
        // Deliver results only when request has been made through the proper channels.
        if (isRequestMade){
            Log.i(TAG, "onLoadFinished: isRequestMade:" + isRequestMade + " Data:" + data.toString());
            oldResults = data;
            view.setLoadIndicatorVisibility(View.GONE);
            onJsonResult.accept(data);
            isRequestMade = false;
        }
        else{
            Log.i(TAG, "onLoadFinished: isRequestMade:" + isRequestMade);
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<List<String>> loader) {
        onJsonResult.accept(null);
    }
    //endregion

}
