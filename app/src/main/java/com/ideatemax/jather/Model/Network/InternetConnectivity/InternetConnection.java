package com.ideatemax.jather.Model.Network.InternetConnectivity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;

public class InternetConnection {

    private InternetConnection() { throw  new AssertionError(); }

    public static boolean hasInternet(Context context) {
        boolean hasInternet = false;
        ConnectivityManager connManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkCapabilities networkCap =
                connManager.getNetworkCapabilities(connManager.getActiveNetwork());
        try {
            hasInternet = networkCap != null &&
                    ( networkCap.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) ||
                            networkCap.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) );
        } catch (NullPointerException e){
            e.printStackTrace();
        }
        return hasInternet;
    }
}
