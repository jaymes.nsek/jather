package com.ideatemax.jather.Model.Network.APIComms;

public interface OnServerResultsListener {
    int FAILED = -1;
    void responseCodeUpdate(int code);
}
