package com.ideatemax.jather.Model.Enums;

import androidx.annotation.NonNull;

import java.util.NoSuchElementException;

public enum Info {
    SERVER,
    DUPLICATE,
    SUCCESS,
    INVALID,
    NO_CITY,
    RETURNED_NULL,
    NO_INTERNET;


    @NonNull
    @Override
    public String toString() {
        switch (this) {
            case SERVER:
                return "Server returned null, check the spelling of your cityDTO.";
            case NO_CITY:
                return "Empty List of Cities";
            case NO_INTERNET:
                return "No internet connection.";
            case SUCCESS:
                return "City added";
            case DUPLICATE:
                return "City already exist";
            case INVALID:
                return "Name must be greater than two characters";
            case RETURNED_NULL:
                return "Incorrect name or no internet connection.";
            default:
                throw new NoSuchElementException();
        }
    }
}
