package com.ideatemax.jather.Activities.CityList;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.loader.app.LoaderManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.ideatemax.jather.Dependencies.DependencyRegistry;
import com.ideatemax.jather.Model.DTOs.CityDTO;
import com.ideatemax.jather.Model.Data.CitiesList.CitiesArrayList;
import com.ideatemax.jather.Model.Enums.Info;
import com.ideatemax.jather.Navigators.RootNavigator;
import com.ideatemax.jather.Helpers.DialogFragments.OnDeleteDialogFragment;
import com.ideatemax.jather.R;
import java.util.ArrayList;
import java.util.List;
import static com.ideatemax.jather.Helpers.Constants.index_generator_key_FS;
import static com.ideatemax.jather.Model.Enums.Info.NO_CITY;
import static com.ideatemax.jather.Model.Enums.Info.NO_INTERNET;
import static com.ideatemax.jather.Model.Enums.Info.RETURNED_NULL;
import static com.ideatemax.jather.Model.Enums.Info.SERVER;
import static com.ideatemax.jather.Model.Enums.Info.SUCCESS;
import static com.ideatemax.jather.Model.Network.InternetConnectivity.InternetConnection.hasInternet;
import static com.ideatemax.jather.Navigators.RootNavigator.ADD_REQUEST;


public class CityActivity extends AppCompatActivity implements CityView,
        OnDeleteDialogFragment.NoticeDialogListener{

    private final String TAG = getClass().getSimpleName();

    private CityListPresenter presenter;
    private RootNavigator navigator;
    private TextView mEmptyStateTextView;
    private FloatingActionButton addFAB;
    private ProgressBar mProgressBar;
    private ListView mListView;

    private SwipeRefreshLayout refreshLayout;
    private ListView.OnItemClickListener mOnItemClickListener;
    private ListView.OnItemLongClickListener mOnItemLongClickListener;

 
    private CitiesArrayList citiesArrayList = new CitiesArrayList();
    private static SharedPreferences preferences;


    //region Lifecycle Hook Methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate: ");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_list);

        preferences = getPreferences(MODE_PRIVATE);

        attachListenersAndUI();

        DependencyRegistry.shared.inject(this, preferences);
    }

    @Override
    protected void onStart() {
        super.onStart();
        setEmptyStateText();
    }

    @Override
    protected void onResume() {
        Log.i(TAG, "onResume: ");
        super.onResume();
    }
    //endregion

    private void setEmptyStateText(){
        Log.i(TAG, "setEmptyStateText: hasInternet:" + hasInternet(this));
        if (hasInternet(this))
            mEmptyStateTextView.setText(getResources().getString(R.string.empty_state_no_city));
        else
            mEmptyStateTextView.setText(getResources().getString(R.string.empty_state_no_internet));
    }

    //region UI Methods
    private void attachListenersAndUI() {
        mListView = findViewById(R.id.list_view_in_main);
        refreshLayout = findViewById(R.id.main_swipe_refresh);
        mEmptyStateTextView = findViewById(R.id.empty_list_textview);
        mProgressBar = findViewById(R.id.loading_indicator);
        addFAB = findViewById(R.id.add_fab);

        addFAB.setOnClickListener(v -> {
            navigator.handleAddClicked(this);
        });

        attachListeners();
        initializeListView();
    }

    //region CallBack Listeners
    private void attachListeners(){
        CitiesArrayList.OnUniqueAddListener uniqueAddListener = this::handleOnUniqueAdd;
        citiesArrayList.setUniqueAddListener(uniqueAddListener);

        mOnItemClickListener = (adapterView, view, i, l) -> itemClicked(i);
        mOnItemLongClickListener = (parent, view, pos, id)
                -> handleOnItemLongClick((CityDTO) mListView.getItemAtPosition(pos), pos);

        mListView.setOnItemClickListener(mOnItemClickListener);
        mListView.setOnItemLongClickListener(mOnItemLongClickListener);
        mListView.setEmptyView(mEmptyStateTextView);

        SwipeRefreshLayout.OnRefreshListener listener = this::handleOnSwipeRefresh;
        refreshLayout.setOnRefreshListener(listener);
    }
    //endregion


    private void itemClicked(int position){
        CityDTO city = (CityDTO) mListView.getItemAtPosition(position);
        navigator.handleOnItemClick(this, city);
    }

    private boolean handleOnItemLongClick(CityDTO city, int itemPosition){
        DialogFragment dialog = new OnDeleteDialogFragment(city.name, itemPosition);
        dialog.show(getSupportFragmentManager(), "OnDeleteDialogFragment" );
        return true;
    }

    /**
     * Main USP of method is that it manages a scenario when refresh is invoke after a loss or
     * absence of network connectivity, hence SharedPref is used for names retrieval.
     */
    private void handleOnSwipeRefresh(){
        int size = citiesArrayList.size();
        if (size == 0) {
            List<String> namesList = presenter.getNamesInSharedPref();
            Log.i(TAG, "handleOnSwipeRefresh: namesList - "+namesList);
            // The scenario where this if statement would be triggered: would be regain of
            // internet after a loss, hence check that internet is present before injection.
            if (namesList != null && namesList.size() > 0) {
                if (hasInternet(this))
                    DependencyRegistry.shared.inject(this, namesList);
                else refreshLayout.setRefreshing(false);
            }
            else{
                refreshLayout.setRefreshing(false); 
            }
        }
        else {
            ArrayList<String> arrayList = new ArrayList<>();
            for (int i = 0; i < size; i++){
                arrayList.add(citiesArrayList.get(i).name);
            }
            DependencyRegistry.shared.inject(this, arrayList);
        }
        setEmptyStateText();
    }

    private void handleOnUniqueAdd(String name){
        Log.i(TAG, "handleOnUniqueAdd: uniqueAddListener entry");
        SharedPreferences.Editor editor = preferences.edit();
        //editor.putInt(name, getIndex());
        // Value portion is not required at present, boolean used as it takes up least memory.
        editor.putBoolean(name, true);
        editor.apply();
        Log.i(TAG, "handleOnUniqueAdd: uniqueAddListener exit");
    }
    //endregion

    //region ListView Adapter
    private void initializeListView() {
        CityArrayAdapter adapter = new CityArrayAdapter(this, citiesArrayList);
        adapter.setNotifyOnChange(true);
        // Set the adapter on ListView such that the list items are populated to screen.
        mListView.setAdapter(adapter);
    }
    //endregion

    //region Injection Methods
    @Override
    public void configureMultipleWith(CityListPresenter presenter, RootNavigator navigator){
        Log.i(TAG, "configureMultipleWith: ");
        this.presenter = presenter;
        this.navigator = navigator;
        loadData();
    }

    @Override
    public void configureSingleWith(CityListPresenter presenter, RootNavigator navigator) {
        Log.i(TAG, "configureSingleWith: ");
        this.presenter = presenter;
        this.navigator = navigator;
        loadData();
    }

    @Override
    public void configureRefreshWith(CityListPresenter presenter) {
        Log.i(TAG, "configureRefreshWith: ");
        this.presenter = presenter;
        //if (citiesArrayList.size() == 0) refreshLayout.setRefreshing(false);
        loadData();
    }
    //endregion


    //region Data Process specific to CityActivity
    private void loadData() {
        presenter.loadData();
    }

    //endregion


    //region Delete Dialog Fragment Overrides
    /**
     * DialogFragment Overrides: The dialog fragments receives a reference to this Activity through
     * the Fragment.onAttach() callback, which it uses to call the following methods defined
     * by the NoticeDialogFragment.NoticeDialogListener interface
     * Manages the side-effects of user clicked the dialog's positive (DELETE) button.
     */
    @Override
    public void onDialogPositiveClick(OnDeleteDialogFragment dialog) {
        // Remove the city from SharedPref + CitiesArrayList.java
        CityDTO city = (CityDTO) mListView.getItemAtPosition(dialog.getItemPosition());

        // Presenter is used instead of setting up an independent Listener for remove() since this
        // Dialog notification itself fulfills that function.
        presenter.removeNameFromSharedPref(city.name);

        CityArrayAdapter adapter = (CityArrayAdapter) mListView.getAdapter();
        assert adapter != null;

        try {
            adapter.remove(city);
        } catch (UnsupportedOperationException e) {
            Log.e(TAG, "onDialogPositiveClick: ", e);
        }

        adapter.notifyDataSetChanged();
    }

    /**
     * Manage the side-effects of user clicked the dialog's negative (Cancel) button.
     */
    @Override
    public void onDialogNegativeClick(OnDeleteDialogFragment dialog) {
        // Do nothing.
    }
    //endregion

    /**
     * Returns the current applicable index that is used for identifying groups of data in SPref.
     */
    @Deprecated
    private int getIndex() {
        // Get current index from countSPref and increment this by one, then return to caller.
        // If key does not exist then return "0" zero as default, and then add count to sPref.
        int currentIndex = preferences.getInt(index_generator_key_FS, 0);

        Log.i(TAG, "getIndex: " + currentIndex);
        // Prepare next index increment SPref index by 1.
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(index_generator_key_FS, currentIndex + 1);
        editor.apply();

        return currentIndex;
    }

    //region Intent Result
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            if (requestCode == ADD_REQUEST){
                Bundle bundle = null;
                try {
                    bundle = data.getExtras();
                }catch (NullPointerException e){
                    e.printStackTrace();
                } finally {
                    DependencyRegistry.shared.inject(this, bundle);
                }
            }
        }
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public LoaderManager getCityLoaderManager() {
        return getSupportLoaderManager();
    }

    @Override
    public void onQueryResult(List<CityDTO> cities) {
        String listOfOldCities = citiesArrayList != null ? citiesArrayList.toString() : "null";
        Log.i(TAG, "onQueryResult: old Cities | " + listOfOldCities );

        String listOfCities = cities != null ? cities.toString() : "null";
        Log.i(TAG, "onQueryResult: new City | " + listOfCities);

        if (cities != null && cities.size() > 0){
            /*// Return when in single
            String name = cities.get(0).name;
            if ( cities.size() == 1 && citiesArrayList.isCityInList(name) ) {
                Log.i(TAG, "onQueryResult: cities.size:" + cities.size() + " name:" + name);
                return;
            }*/

            CityArrayAdapter adapter = (CityArrayAdapter) mListView.getAdapter();
            assert adapter != null;
            adapter.addAll(cities);
            adapter.notifyDataSetChanged();
        }

        // Enforce refresh UI circular arrow is ended on result.
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void reportFinishStatus(Info info) {
        String msg;

        switch (info) {
            case NO_CITY:
                msg = NO_CITY.toString();
                break;
            case SERVER:
                msg = SERVER.toString();
                break;
            case RETURNED_NULL:
                msg = RETURNED_NULL.toString();
                break;
            case NO_INTERNET:
                msg = NO_INTERNET.toString();
                break;
            case SUCCESS:
                msg = SUCCESS.toString();
                break;
            default: return;
        }
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setLoadIndicatorVisibility(int visibility) {
        mProgressBar.setVisibility(visibility);
    }
    //endregion

}
