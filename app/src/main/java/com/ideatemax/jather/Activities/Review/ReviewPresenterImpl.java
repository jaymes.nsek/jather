package com.ideatemax.jather.Activities.Review;

import android.content.Context;
import android.os.Bundle;
import com.ideatemax.jather.Helpers.Formatter;
import com.ideatemax.jather.Model.ModelLayer;
import static com.ideatemax.jather.Helpers.Constants.EXTRA_HUMIDITY;
import static com.ideatemax.jather.Helpers.Constants.EXTRA_NAME;
import static com.ideatemax.jather.Helpers.Constants.EXTRA_TEMPERATURE;
import static com.ideatemax.jather.Helpers.Constants.EXTRA_UNIX_TIME;
import static com.ideatemax.jather.Helpers.Constants.EXTRA_WEATHER_DESC;


public class ReviewPresenterImpl implements ReviewPresenter{
    
    private final String TAG = getClass().getSimpleName(); 

    public String name;
    public String temperature;
    public String humidity;
    public String weatherDescription;
    public String issueInfo;

    private Formatter formatter;
    private ModelLayer modelLayer;
    private Bundle bundle;


    public ReviewPresenterImpl(Context context, Bundle bundle, ModelLayer modelLayer) {
        this.modelLayer = modelLayer;
        this.bundle = bundle;

        formatter = new Formatter();

        formatData(context);
    }

    private void formatData(Context context) {
        name = bundle.getString(EXTRA_NAME);
        humidity = formatter.formatHumidity(context, bundle.getInt(EXTRA_HUMIDITY));
        temperature = formatter.formatTemp(context, bundle.getFloat(EXTRA_TEMPERATURE));
        weatherDescription = bundle.getString(EXTRA_WEATHER_DESC);
        issueInfo = formatter.formatIssueInfo(context, bundle.getLong(EXTRA_UNIX_TIME));
    }

    //region Getters
    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getHumidity() {
        return humidity;
    }

    @Override
    public String getTemperature() {
        return temperature;
    }

    @Override
    public String getDescription() {
        return weatherDescription;
    }

    @Override
    public String getIssueInfo() {
        return issueInfo;
    }
    //endregion
}
