package com.ideatemax.jather.Activities.Add;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;
import com.ideatemax.jather.Model.Enums.Info;
import com.ideatemax.jather.Model.ModelLayer;
import static com.ideatemax.jather.Model.Enums.Info.DUPLICATE;
import static com.ideatemax.jather.Model.Enums.Info.INVALID;
import static com.ideatemax.jather.Model.Enums.Info.NO_INTERNET;
import static com.ideatemax.jather.Model.Enums.Info.SUCCESS;
import static com.ideatemax.jather.Model.Network.InternetConnectivity.InternetConnection.hasInternet;


public class AddPresenterImpl implements  AddPresenter{

    private final String TAG = getClass().getSimpleName();
    private ModelLayer modelLayer;

    public AddPresenterImpl(ModelLayer modelLayer) {
        this.modelLayer = modelLayer;
    }

    @Override
    public Info getStatusReport(Context context, String name) {
        if (!hasInternet(context)){
            Log.i(TAG, "getStatusReport: No Internet");
            return NO_INTERNET;
        }
        else if (name.equals("") || name.length() <= 2){
            Log.i(TAG, "getStatusReport: INVALID");
            return INVALID;
        }
        else if (modelLayer.isCityInSharedPref(name)) {
            Log.i(TAG, "getStatusReport: DUPLICATE" + name);
            return DUPLICATE;
        }
        else {
            Log.i(TAG, "getStatusReport: SUCCESS");
            return SUCCESS;
        }
    }

    @Override
    public void showStatusToast(Context context, Info info){
        String msg;

        switch (info) {
            case DUPLICATE:
                msg = DUPLICATE.toString();
                break;
            case INVALID:
                msg = INVALID.toString();
                break;
            case NO_INTERNET:
                msg = NO_INTERNET.toString();
                break;
            default:
                msg = "Nothing to print";
        }
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }
}
