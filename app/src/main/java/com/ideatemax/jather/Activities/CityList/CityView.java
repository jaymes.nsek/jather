package com.ideatemax.jather.Activities.CityList;

import android.content.Context;

import androidx.loader.app.LoaderManager;

import com.ideatemax.jather.Model.DTOs.CityDTO;
import com.ideatemax.jather.Model.Enums.Info;
import com.ideatemax.jather.Navigators.RootNavigator;

import java.util.List;

public interface CityView extends  CityViewMini {
    void configureMultipleWith(CityListPresenter presenter, RootNavigator navigator);
    void configureSingleWith(CityListPresenter presenter, RootNavigator navigator);
    void configureRefreshWith(CityListPresenter presenter);
}
