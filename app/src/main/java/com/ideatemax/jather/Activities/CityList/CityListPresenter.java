package com.ideatemax.jather.Activities.CityList;

import java.util.List;

public interface CityListPresenter {
    void loadData();
    void removeNameFromSharedPref(String name);
    List<String> getNamesInSharedPref();
}
