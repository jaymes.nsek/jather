package com.ideatemax.jather.Activities.CityList;

import com.ideatemax.jather.Model.ModelLayer;

import java.util.List;


public class CityListPresenterImpl implements CityListPresenter {

    public final String TAG = getClass().getSimpleName();
    private ModelLayer modelLayer;
    CityViewMini viewMini;

    public CityListPresenterImpl(CityViewMini viewMini, ModelLayer modelLayer) {
        this.modelLayer = modelLayer;
        this.viewMini = viewMini;
    }

    //region Presenter Methods
    @Override
    public void loadData() {
        modelLayer.loadData(viewMini);
    }

    @Override
    public void removeNameFromSharedPref(String name) {
        modelLayer.removeNameFromSharedPref(name);
    }

    @Override
    public List<String> getNamesInSharedPref() {
        return modelLayer.getNamesInSharedPref();
    }
    //endregion

}
