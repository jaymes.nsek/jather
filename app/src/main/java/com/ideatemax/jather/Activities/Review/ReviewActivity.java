package com.ideatemax.jather.Activities.Review;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import com.ideatemax.jather.Dependencies.DependencyRegistry;
import com.ideatemax.jather.Navigators.RootNavigator;
import com.ideatemax.jather.R;


/**
 * The role of this class is to display more data store to the CityDTO object when the corresponding
 * listItem is clicked on the CityActivity.
 */
public class ReviewActivity extends AppCompatActivity {

    private final String TAG = getClass().getSimpleName();

    private ReviewPresenter presenter;
    private RootNavigator navigator;
    private TextView mNameTextView;
    private TextView mHumidityTextView;
    private TextView mTempTextView;
    private TextView mDescTextView;
    private TextView mIssueInfoTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);

        attachUI();
        Bundle bundle = getIntent().getExtras();
        DependencyRegistry.shared.inject(this, bundle);
    }

    /**
     * Method inflates the menu_toolbar resources.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_toolbar, menu);
        return true;
    }

    private void attachOnBackPressedDispatcher(){
        // Enable and manage onBackPressed.
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
               startCityListActivity();
            }
        };
        this.getOnBackPressedDispatcher().addCallback(this, callback);
    }

    private void startCityListActivity(){
        navigator.handleCityListActivityLaunch(this);
    }


    //region UI Methods
    private void attachUI(){
        mNameTextView = findViewById(R.id.review_city_name);
        mHumidityTextView = findViewById(R.id.review_humidity_text_view);
        mTempTextView = findViewById(R.id.review_temp_text_view);
        mDescTextView = findViewById(R.id.review_desc);
        mIssueInfoTextView = findViewById(R.id.review_issued_time);

        attachActionBarSupport();
        attachOnBackPressedDispatcher();
    }

    private void attachActionBarSupport(){
        // Inflate menu to Toolbar.
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*try {
            // Get a support ActionBar corresponding to this toolbar
            ActionBar ab = getSupportActionBar();

            assert ab != null;
            ab.setDisplayHomeAsUpEnabled(true);

        }catch (Exception e){
            Log.e(TAG, "onCreate: ", e);
        }*/
    }

    /**
     * Procedure to undertake when corresponding menu items are clicked.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_item_add_city:
                navigator.handleAddActivityLaunch(this);
                break;
            case R.id.menu_item_homepage:
            default:
                navigator.handleCityListActivityLaunch(this);
        }

        return true;
    }
    //endregion

    //region Injection Methods
    public void configureWith(ReviewPresenter presenter, RootNavigator navigator) {
        this.presenter = presenter;
        this.navigator = navigator;
        setViews();
    }

    private void setViews(){
        mNameTextView.setText(presenter.getName());
        mHumidityTextView.setText(presenter.getHumidity());
        mTempTextView.setText(presenter.getTemperature());
        mDescTextView.setText(presenter.getDescription());
        mIssueInfoTextView.setText(presenter.getIssueInfo());
    }
    //endregion

}
