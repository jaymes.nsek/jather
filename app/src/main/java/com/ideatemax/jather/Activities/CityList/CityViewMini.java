package com.ideatemax.jather.Activities.CityList;

import android.content.Context;
import android.net.NetworkInfo;

import androidx.loader.app.LoaderManager;

import com.ideatemax.jather.Model.DTOs.CityDTO;
import com.ideatemax.jather.Model.Enums.Info;

import java.util.List;

public interface CityViewMini {
    Context getContext();
    LoaderManager getCityLoaderManager();
    void onQueryResult(List<CityDTO> cities);
    void reportFinishStatus(Info info);
    void setLoadIndicatorVisibility(int visibility);
}
