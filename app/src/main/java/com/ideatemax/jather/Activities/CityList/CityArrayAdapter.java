package com.ideatemax.jather.Activities.CityList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.ideatemax.jather.Helpers.Formatter;
import com.ideatemax.jather.Model.DTOs.CityDTO;
import com.ideatemax.jather.R;
import java.util.List;


/**
 * Class's responsibility is to Adapt the List content for the ListView
 */
public class CityArrayAdapter extends ArrayAdapter<CityDTO> {

    private int colorWhite;
    private Formatter formatter;
    private boolean isOddPosition;

    /**
     * @param cities  is the list of cities stored in the ArrayList, which is the data to
     *                adapt to the ListView.
     */
    public CityArrayAdapter(@NonNull Context context, @NonNull List<CityDTO> cities) {
        super(context, 0, cities);
        colorWhite = getContext().getResources().getColor(android.R.color.white, null);
        formatter = new Formatter();
    }

    /**
     * Returns a list item view that displays info about the weather at the given position
     *      in the list of cities.
     * @param position is the current request position in the ArrayList.
     * @param convertView is a reused view if it exists otherwise NULL.
     * @return is the adapted view.
     */
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        // Retrieve the city at the given position in the ArrayList of Cities.
        CityDTO city =  getItem(position);

        // Check if the existing View is being reused, otherwise inflate it.
        View listItemView = convertView;
        if (listItemView == null){
            listItemView = LayoutInflater.from(getContext())
                    .inflate(R.layout.city_list_item, parent, false);
        }

        isOddPosition = position % 2 == 0;
        if(isOddPosition) {
            listItemView.setBackgroundColor(ContextCompat.getColor(getContext(),
                    R.color.light_blue));
        }

        assert city != null : "getName returns NULL" ;
        setTextViewParams(listItemView.findViewById(R.id.list_item_city_tv), city.name);
        setTextViewParams(listItemView.findViewById(R.id.list_item_temp_tv),
                formatter.formatTemp(getContext(), city.tempInCelsius));
        setTextViewParams(listItemView.findViewById(R.id.list_item_last_updated_tv),
                formatter.computeLastUpdatedTime(getContext(), city.timeInUnixSeconds));

        return listItemView;
    }

    //region Helper Method
    private void setTextViewParams(TextView view, String text){
        view.setText(text);
        if (isOddPosition) view.setTextColor(colorWhite);
    }
    //endregion

}
