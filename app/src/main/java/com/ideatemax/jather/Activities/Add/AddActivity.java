package com.ideatemax.jather.Activities.Add;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import com.ideatemax.jather.Dependencies.DependencyRegistry;
import com.ideatemax.jather.Model.Enums.Info;
import com.ideatemax.jather.Navigators.RootNavigator;
import com.ideatemax.jather.R;
import static com.ideatemax.jather.Model.Enums.Info.DUPLICATE;
import static com.ideatemax.jather.Model.Enums.Info.INVALID;
import static com.ideatemax.jather.Model.Enums.Info.NO_INTERNET;
import static com.ideatemax.jather.Model.Enums.Info.SUCCESS;


/**
 * Activity class is used for adding cities to the Application, and it role is one of a secondary
 * bridge between the device, APIAsyncTask.java and the server.
 */
public class AddActivity extends AppCompatActivity {

    private final String TAG = getClass().getSimpleName();
    private AddPresenter presenter;
    private RootNavigator navigator;
    private Button finishButton;
    private Button cancelButton;
    private EditText addEditText;
    // Used for ensuring that Activity does not receive multiple click inputs.
    public static boolean isFinishClickRegistered;

    //region ALC Hook Methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        isFinishClickRegistered = false;
        attachUI();

        DependencyRegistry.shared.inject(this);
    }
    //endregion


    //region User Interaction
    private void attachUI(){
        cancelButton = findViewById(R.id.add_cancel_btn);
        finishButton = findViewById(R.id.add_finish_btn);
        addEditText = findViewById(R.id.add_edit_text);

        cancelButton.setOnClickListener(v -> onCancelClicked());
        finishButton.setOnClickListener(v -> onFinishClicked());

        attachOnBackPressedDispatcher();
    }

    private void attachOnBackPressedDispatcher(){
        // Enable and manage onBackPressed.
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                startCityListActivity();
            }
        };
        this.getOnBackPressedDispatcher().addCallback(this, callback);
    }

    private void startCityListActivity(){
        navigator.handleCityListActivityLaunch(this);
    }
    //endregion


    //region User Interactions
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private void onFinishClicked() {
        Info info;
        if (!isFinishClickRegistered) {
            String cityName = addEditText.getText().toString();
            // Set click registered true so that no other inputs can interrupt the process.
            isFinishClickRegistered = true;
            info = presenter.getStatusReport(this, cityName);

            if (info != null){
                if (info == NO_INTERNET || info == DUPLICATE || info == INVALID)
                    presenter.showStatusToast(this, info);
                else if (info == SUCCESS)
                    navigator.handleFinishedClicked(this, cityName);
            }
            isFinishClickRegistered = false;
        }
    }

    public void onCancelClicked(){
        navigator.handleCityListActivityLaunch(this);
    }
    //endregion

    public void configureWith(AddPresenter presenter, RootNavigator navigator) {
        this.presenter = presenter;
        this.navigator = navigator;
    }



}
