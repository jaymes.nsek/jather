package com.ideatemax.jather.Activities.Review;

/**
 * For the presenter all objects are render in String for direct use with TextView.setText(),
 * however, resourceID's should be of type int.
 */
public interface ReviewPresenter {
    String getName();

    String getHumidity();

    String getTemperature();

    String getDescription();

    String getIssueInfo();
}
