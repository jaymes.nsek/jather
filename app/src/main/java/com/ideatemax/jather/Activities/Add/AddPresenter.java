package com.ideatemax.jather.Activities.Add;

import android.content.Context;

import androidx.core.util.Consumer;

import com.ideatemax.jather.Model.Enums.Info;

public interface AddPresenter {
    void showStatusToast(Context context, Info info);
    Info getStatusReport(Context context, String name);
}
