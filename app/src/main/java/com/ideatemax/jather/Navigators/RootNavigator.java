package com.ideatemax.jather.Navigators;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.ideatemax.jather.Activities.Add.AddActivity;
import com.ideatemax.jather.Activities.CityList.CityActivity;
import com.ideatemax.jather.Activities.Review.ReviewActivity;
import com.ideatemax.jather.Model.DTOs.CityDTO;

import static com.ideatemax.jather.Helpers.Constants.EXTRA_HUMIDITY;
import static com.ideatemax.jather.Helpers.Constants.EXTRA_NAME;
import static com.ideatemax.jather.Helpers.Constants.EXTRA_TEMPERATURE;
import static com.ideatemax.jather.Helpers.Constants.EXTRA_UNIX_TIME;
import static com.ideatemax.jather.Helpers.Constants.EXTRA_WEATHER_DESC;


public class RootNavigator {

    private final String TAG = getClass().getSimpleName();
    public final static int ADD_REQUEST = 123;

    //region CityActivity
    public void handleOnItemClick(CityActivity activity, CityDTO city){
        Intent intent = new Intent(activity, ReviewActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_NAME, city.name);
        bundle.putInt(EXTRA_HUMIDITY, city.humidity);
        bundle.putFloat(EXTRA_TEMPERATURE, city.tempInCelsius);
        bundle.putString(EXTRA_WEATHER_DESC, city.weatherDescription);
        bundle.putLong(EXTRA_UNIX_TIME, city.timeInUnixSeconds);
        //bundle.putInt(EXTRA_ITEM_POSITION, );

        intent.putExtras(bundle);
        activity.startActivity(intent);
    }

    /**
     * Method explicitly starts AddActivity for results when the add FAB is clicked.
     */
    public void handleAddClicked(CityActivity activity){
        Intent intent = new Intent(activity, AddActivity.class);
        activity.startActivityForResult(intent, ADD_REQUEST);

        /*Intent intent = new Intent(activity, AddActivity.class);
        activity.startActivity(intent);*/
    }
    //endregion

    //region AddActivity
    public void handleFinishedClicked(AddActivity activity, String cityName) {
        Intent intent = new Intent(activity, CityActivity.class);
        intent.putExtra(EXTRA_NAME, cityName);
        activity.setResult(Activity.RESULT_OK, intent);
        activity.finish();
        Log.i(TAG, "handleFinishedClicked: setResult called");
    }
    //endregion

    //region ReviewActivity
    /**
     * Helper for starting CityActivity via intent.
     */
    public void handleAddActivityLaunch(ReviewActivity activity){
        Intent intent = new Intent(activity, AddActivity.class);
        activity.startActivity(intent);
    }
    //endregion

    //region Add + Review Activities
    public void handleCityListActivityLaunch(Activity activity) {
        // No Category is added so that activity.configureForNavigation() is used in DependencyReg.
        Intent intent = new Intent(activity, CityActivity.class);
        activity.startActivity(intent);
        activity.finish();
    }
    //endregion
}
