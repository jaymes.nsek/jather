package com.ideatemax.jather.Helpers.DialogFragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import com.ideatemax.jather.R;
import java.util.Objects;


/**
 * Class for managing confirmation of delete request.
 */
public class OnDeleteDialogFragment extends DialogFragment {

    private String mCityName;
    private int mItemPosition;
    // Use this instance of the interface to deliver action events - composed in onCreateDialog()
    private NoticeDialogListener listener;


    /**
     * Public constructor takes param mCityName with which to operate on.
     */
    public OnDeleteDialogFragment(String cityName, int itemPosition) {
        this.mCityName = cityName;
        this.mItemPosition = itemPosition;
    }

    /**
     * Overridden method displays an AlertDialog to prompt for a delete confirmation.
     * @param savedInstanceState The last saved instance state of the Fragment,
     *                           or null if this is a freshly created Fragment.
     * @return Return a new Dialog instance to be displayed by the Fragment.
     */
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        //return super.onCreateDialog(savedInstanceState);

        // Build the dialog and set up the button click handlers
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()),
                R.style.Theme_AppCompat_DayNight_Dialog_Alert);

        // Build dialog msg with mCityName's name.

        String msg = this.getResources().getString(R.string.on_delete_frag_msg, mCityName);

        builder.setMessage(msg)
                .setPositiveButton(R.string.onDeleteFrag_P, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Send the positive button event back to the host activity
                        listener.onDialogPositiveClick(OnDeleteDialogFragment.this);
                    }
                })
                .setNegativeButton(R.string.onDeleteFrag_N, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Send the negative button event back to the host activity
                        listener.onDialogNegativeClick(OnDeleteDialogFragment.this);
                    }
                });
        return builder.create();
    }


    /**
     * Nested Interface: OnDeleteDialogFragment.NoticeDialogListener - The activity that creates
     * an instance of this dialog fragment must implement this interface in order to receive event
     * callbacks. Each method passes the DialogFragment in case the host needs to query it.
     */
    public interface NoticeDialogListener {
        void onDialogPositiveClick(OnDeleteDialogFragment dialog);
        void onDialogNegativeClick(OnDeleteDialogFragment dialog);
    }


    /**
     * Override the Fragment.onAttach() method to instantiate the NoticeDialogListener.
     * @param context is the App Context interacting with the DialogFragment.
     */
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        // Verify that the host activity implements the callback interface.
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host.
            listener = (NoticeDialogListener) context;
        } catch (ClassCastException e) {
            // Throw E if Activity doesn't implement the interface.
            throw new ClassCastException(this.toString() + " must implement NoticeDialogListener");
        }
    }


    /**
     * Method returns the associated city name that was sent to the DialogFragment.
     */
    public int getItemPosition() {
        return mItemPosition;
    }

}
