package com.ideatemax.jather.Helpers;

import android.content.Context;

import com.ideatemax.jather.R;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;


/**
 * The role of this class is to format all data (esp in CityArrayAdapter) retrieves from API and
 * stored in CityDTO.java. It ensures that if Adapter data requires formatting in future, it is
 * independent of the data retrieval process.
 */
public class Formatter {

    private final String TAG = getClass().getSimpleName();

    public Formatter() { }

    /**
     * Helper method rounds up Celsius to nearest whole number and then types it toString.
     * @param tempInCelsius is the numerical tempInCelsius to convert.
     * @return is the string tempInCelsius.
     */
    public String formatTemp(Context context, float tempInCelsius){
        // Round number to nearest whole number.
        return context.getResources().getString(R.string.temp_in_celsius,
                new DecimalFormat("0").format(tempInCelsius));
    }

    public String formatIssueInfo(Context context, long timeInUnixSeconds){
        return context.getResources().getString(R.string.utils_issued_info,
                this.interpolateIssuedDate(timeInUnixSeconds),
                this.interpolateIssuedTime(timeInUnixSeconds));
    }

    public String formatHumidity(Context context, int humidity){
        return context.getResources().getString(R.string.utils_humidity, humidity +"%");
    }


    /**
     * Interpolate issue date from Unix long data.
     * @param unixTimeSeconds ís the number of seconds from that have elapsed from 1 January 1970
     *                      until the weather info was issued.
     * @return is the string date in "MMM DD, yyyy" format.
     */
    public String interpolateIssuedDate(long unixTimeSeconds){
        return new SimpleDateFormat("MMM dd, yyyy")
                .format(new java.util.Date (unixTimeSeconds*1000));
    }


    /**
     * Interpolate issue date from Unix long data.
     * @param unixTimeSeconds ís the number of seconds from that have elapsed from 1 January 1970
     *                      until the weather info was issued.
     * @return is the string date in "h:mm a" format.
     */
    public String interpolateIssuedTime(long unixTimeSeconds){
        // Configure a SimpleDateFormat for the time representation.
        return new SimpleDateFormat("h:mm a")
                .format(new java.util.Date (unixTimeSeconds*1000));
    }

    public String computeLastUpdatedTime(Context context, long unixTimeStamp){
        long t = timeInMinSinceLastUpdate(unixTimeStamp);
        if (t <= 1){
            return context.getResources().getString(R.string.updated_a_moment_ago);
        }
        else{
            return context.getResources().getString(R.string.lastUpdatedTime, String.valueOf(t));
        }
    }

    private long timeInMinSinceLastUpdate(long unixTimeStamp){
        return ((System.currentTimeMillis()/1000) - unixTimeStamp) / 60;
    }
}
