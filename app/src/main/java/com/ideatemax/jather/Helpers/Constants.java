package com.ideatemax.jather.Helpers;

public class Constants {
    //region SharedPreferences
    public static final String SHARED_PREF_HANDLE = "com.ideatemax.jather.shared_pref";
    public final static String index_generator_key_FS = "com.ideatemax.myKey01";
    //endregion

    //region Intent Extras
    public static final String EXTRA_NAME = "com.ideatemax.jather.name";
    public static final String EXTRA_HUMIDITY = "com.ideatemax.jather.humidity";
    public static final String EXTRA_TEMPERATURE = "com.ideatemax.jather.temperature";
    public static final String EXTRA_WEATHER_DESC = "com.ideatemax.jather.description";
    public static final String EXTRA_ISSUE_DATE_N_TIME = "com.ideatemax.jather.issue_details";
    public static final String EXTRA_UNIX_TIME = "com.ideatemax.jather.unix_time";
    public static final String EXTRA_ITEM_POSITION = "com.ideatemax.jather.position";
    //endregion

    //region API
    // JSON Response - URL to query the Open Weather (OP) API for city's weather information.
    public static final String OW_URL_REQUEST_STEM =
            "http://api.openweathermap.org/data/2.5/weather?";
    // Location & Metric queries need to be appended to this, e.g. q=London
    public static final String OW_METRIC_FORMAT = "units=metric";
    // API Key
    public static final String OW_API_KEY = "appid=d57bc41ffb529c499e0ad5eb5e981796";
    //endregion

    public static final int LOADER_ID = 0;
}
