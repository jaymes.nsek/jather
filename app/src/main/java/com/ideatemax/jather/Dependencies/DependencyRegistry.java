package com.ideatemax.jather.Dependencies;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.ideatemax.jather.Activities.Add.AddActivity;
import com.ideatemax.jather.Activities.Add.AddPresenter;
import com.ideatemax.jather.Activities.Add.AddPresenterImpl;
import com.ideatemax.jather.Activities.CityList.CityActivity;
import com.ideatemax.jather.Activities.CityList.CityListPresenter;
import com.ideatemax.jather.Activities.CityList.CityListPresenterImpl;
import com.ideatemax.jather.Activities.CityList.CityView;
import com.ideatemax.jather.Activities.Review.ReviewActivity;
import com.ideatemax.jather.Activities.Review.ReviewPresenter;
import com.ideatemax.jather.Activities.Review.ReviewPresenterImpl;
import com.ideatemax.jather.Helpers.Constants;
import com.ideatemax.jather.Model.Data.DataLayer;
import com.ideatemax.jather.Model.Data.DataLayerImpl;
import com.ideatemax.jather.Model.ModelLayer;
import com.ideatemax.jather.Model.ModelLayerImpl;
import com.ideatemax.jather.Model.Network.NetworkLayer;
import com.ideatemax.jather.Model.Network.NetworkLayerImpl;
import com.ideatemax.jather.Navigators.RootNavigator;
import com.ideatemax.jather.Translation.TranslationLayer;
import com.ideatemax.jather.Translation.TranslationLayerImpl;

import java.util.ArrayList;
import java.util.List;

import static com.ideatemax.jather.Model.Enums.ProcessType.MULTIPLE;
import static com.ideatemax.jather.Model.Enums.ProcessType.REFRESH;
import static com.ideatemax.jather.Model.Enums.ProcessType.SINGLE;


public final class DependencyRegistry {

    private final String TAG = getClass().getSimpleName();

    public static DependencyRegistry shared = new DependencyRegistry();
    private CityListPresenter cityListPresenter;

    //region Navigators
    public RootNavigator rootNavigator = new RootNavigator();
    //endregion

    //region Singletons
    TranslationLayer translationLayer = new TranslationLayerImpl();

    public DataLayer dataLayer = null;
    private DataLayer createDataLayer(SharedPreferences preference) {
        return new DataLayerImpl(preference);
    }

    public NetworkLayer networkLayer = new NetworkLayerImpl();

    public ModelLayer modelLayer;
    private ModelLayer createModelLayer(SharedPreferences preferences) {
        dataLayer = createDataLayer(preferences);
        return new ModelLayerImpl(networkLayer, dataLayer, translationLayer);
    }
    //endregion


    private DependencyRegistry() {

    }

    //region Injection Methods

    public void inject(CityView view, SharedPreferences preferences) {
        if (dataLayer == null){
            modelLayer = createModelLayer(preferences);
        }
        modelLayer.setProcessTypeAndNames(MULTIPLE, null);
        cityListPresenter = new CityListPresenterImpl(view, modelLayer);
        view.configureMultipleWith(cityListPresenter, rootNavigator);
    }

    public void inject(CityView view, Bundle bundle) {
        assert dataLayer != null : "DataLayer must be non null";
        // Flagging processType determines path taken in ModelLayerIMpl, i.e. whether
        // passed name from AddActivity is used or load...FromLocal() to get SharedPref data.
        modelLayer.setProcessTypeAndNames(SINGLE, nameListFromBundle(bundle));
        cityListPresenter = new CityListPresenterImpl(view, modelLayer);
        view.configureSingleWith(cityListPresenter, rootNavigator);
    }

    public void inject(CityView view, List<String> arrayList){
        modelLayer.setProcessTypeAndNames(REFRESH, arrayList);
        cityListPresenter = new CityListPresenterImpl(view, modelLayer);
        view.configureRefreshWith(cityListPresenter);
    }

    public void inject(AddActivity activity){
        AddPresenter presenter = new AddPresenterImpl(modelLayer);
        activity.configureWith(presenter, rootNavigator);
    }

    public void inject(ReviewActivity activity, Bundle bundle){
        ReviewPresenter presenter = new ReviewPresenterImpl(activity, bundle, modelLayer);
        activity.configureWith(presenter, rootNavigator);
    }
    //endregion


    //region Helper Methods
    private List<String> nameListFromBundle(Bundle bundle) {
        if(bundle == null) return null;
        Log.i(TAG, "nameListFromBundle: " + bundle.getString(Constants.EXTRA_NAME));
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add(bundle.getString(Constants.EXTRA_NAME));
        return arrayList;
    }
    //endregion

}
