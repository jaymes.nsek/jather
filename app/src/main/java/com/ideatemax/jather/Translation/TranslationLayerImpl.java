package com.ideatemax.jather.Translation;

import android.text.TextUtils;
import android.util.Log;

import com.ideatemax.jather.Model.DTOs.CityDTO;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TranslationLayerImpl implements TranslationLayer {

    private final String TAG = getClass().getSimpleName();

    public TranslationLayerImpl() {
    }

    /**
     * Extract necessary fields from the JSONResponse, creates a CityDTO obj from these and
     * returns to caller.
     */
    @Override
    public List<CityDTO> translate(List<String> jsonResponse) {
        List<CityDTO> dTos = new ArrayList<>();

        for (String json : jsonResponse){
            dTos.add(jsonToCityDTO(json));
        }
        return dTos;
    }

    private CityDTO jsonToCityDTO(String json){
        if (TextUtils.equals(json, null) || TextUtils.isEmpty(json)) return null;

        try{
            JSONObject rootJsonResponse = new JSONObject(json);
            String name = rootJsonResponse.getString("name");

            JSONObject mainJsonBranch = rootJsonResponse.getJSONObject("main");
            float temp = (float) mainJsonBranch.getDouble("temp");
            int humid = mainJsonBranch.getInt("humidity");

            JSONArray weatherJsonBranch = rootJsonResponse.getJSONArray("weather");
            JSONObject firstElementInBranch = weatherJsonBranch.getJSONObject(0);
            String desc = firstElementInBranch.getString("description");
            long timeInMs = rootJsonResponse.getLong("dt");

            Log.i(TAG, "extractFieldsFromJson: Values - " + " " + name + " " +
                    temp + " " +  humid + " " +  desc + " " + timeInMs);

            return new CityDTO(name, temp, humid, desc, timeInMs);
        } catch (JSONException e){
            Log.e(TAG, "Error parsing city JSON results", e);
            return null;
        }
    }

}
