package com.ideatemax.jather.Translation;

import com.ideatemax.jather.Model.DTOs.CityDTO;

import java.util.List;

public interface TranslationLayer {
    List<CityDTO> translate(List<String> jsonResponse);
}
