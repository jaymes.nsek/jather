package com.ideatemax.jather;

import org.junit.Before;
import org.junit.Test;

import java.net.MalformedURLException;
import java.net.URL;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

    private URL[] citiesURLArr = null;
    private URL[] oldCitiesURLArr = null;

    @Before
    public void setUp() throws Exception {

    }

    /*@Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }*/

    @Test
    public void isDuplicateRequestTest() throws MalformedURLException {
        URL[] url = {new URL("http://api.openweathermap.org/data/2.5/weather?q=Utah&units=metric&appid=d57bc41ffb529c499e0ad5eb5e981796")};

        setOldCitiesURLArr();
        citiesURLArr = url;
        assertFalse(isDuplicateRequest());

        setOldCitiesURLArr();
        citiesURLArr = url;
        assertTrue(isDuplicateRequest());

        URL[] aNewUrl = {
                new URL("http://api.openweathermap.org/data/2.5/weather?q=London&units=metric&appid=d57bc41ffb529c499e0ad5eb5e981796"),
                new URL("http://api.openweathermap.org/data/2.5/weather?q=Utah&units=metric&appid=d57bc41ffb529c499e0ad5eb5e981796")
        };

        setOldCitiesURLArr();
        citiesURLArr = aNewUrl;
        assertFalse(isDuplicateRequest());
    }



    private boolean isDuplicateRequest(){
        boolean isDuplicateCity = false;

        if ( citiesURLArr == null || oldCitiesURLArr == null ) return false;

        for (URL aNewCityURL : citiesURLArr){
            for (URL anOldCityURL : oldCitiesURLArr){
                if (aNewCityURL.toString().equalsIgnoreCase(anOldCityURL.toString())) {
                    isDuplicateCity = true;
                    // Break from inner loop once a true case is flagged.
                    break;
                }
            }
            if (!isDuplicateCity) return false;
        }
        return isDuplicateCity;
    }

    /**
     * Adopts the previous cities list as oldCitiesURLArr.
     */
    private void setOldCitiesURLArr(){
        if (citiesURLArr == null) {
            oldCitiesURLArr = null;
            return;
        }
        oldCitiesURLArr = new URL[citiesURLArr.length];
        System.arraycopy(citiesURLArr, 0, oldCitiesURLArr, 0, citiesURLArr.length);
    }

}